# gitlab-ci-debug-medium

Accompanying repository for Medium article: ["5 ways that can help you to debug your GitLab pipeline"](https://medium.com/geekculture/5-ways-that-can-help-you-to-debug-your-gitlab-pipeline-b871fd626652).

I hope this helps you; thank you for reading!

Take care, and I hope to see you soon.

Kind reagards,

Suru Dissanaike

[www.himinds.com](https://www.himinds.com)

[www.inserttalent.com](https://web.inserttalent.com)




