#!/bin/bash

function exec_cmd()
{
	echo "executing $@"
	$@ >/dev/null 2>&1

	if [ $? -ne 0 ]; then
		echo "Failed to execute $@"
		exit 1
	fi
}


apt update
apt install sl -qq -y
apt install fortune -qq -y
apt install cowsay -qq -y

apt install supervisor -qq -y


cp ./scripts/supervisord.conf /etc/supervisor/conf.d/

echo "preparation script"

pwd


